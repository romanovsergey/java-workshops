import java.io.Serializable;
import java.util.Objects;

public class Derived extends Base {
    private final String additionalDescription;

    public Derived(String description, String additionalDescription) {
        super(description);
        this.additionalDescription = additionalDescription;
    }

    public String getAdditionalDescription() {
        return additionalDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Derived derived = (Derived) o;
        return Objects.equals(additionalDescription, derived.additionalDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), additionalDescription);
    }

    @Override
    public String toString() {
        return "Derived{" +
                "additionalDescription='" + additionalDescription + '\'' +
                '}';
    }
}