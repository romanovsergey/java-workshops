import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Service {
    @NotNull
    public static List<String> getInfo(@NotNull List<Base> objects) {
        List<String> out = new ArrayList<>(objects.size());
        for (Base obj : objects) {
            String className = obj.getClass().getSimpleName();
            if ("Base".equals(className))
                out.add(obj.getDescription());
            else if ("Derived".equals(className))
                out.add(obj.getDescription() + ", " + ((Derived) obj).getAdditionalDescription());
        }
        return out;
    }

    public static boolean isFuncInterface(@NotNull Class<?> cls) {
        if (cls.isInterface()) {
            boolean flag = false;
            Method[] methods = cls.getMethods();
            for (Method mt : methods) {
                int mdf = mt.getModifiers();
                if (Modifier.isPublic(mdf) && Modifier.isAbstract(mdf)) {
                    if (!flag)
                        flag = true;
                    else return false;
                }
            }
            return flag;
        }
        return false;
    }

    public static boolean isSerializable(@NotNull Object obj) {
        return obj instanceof Serializable;
    }

    @NotNull
    public static List<String> havingStaticMethod(@NotNull List<Class<?>> classList) {
        List<String> out = new ArrayList<>(classList.size());
        for (Class<?> cls : classList) {
            Method[] methods = cls.getMethods();
            for (Method mt : methods) {
                if (Modifier.isStatic(mt.getModifiers())) {
                    out.add(cls.getCanonicalName());
                    break;
                }
            }
        }
        return out;
    }
}