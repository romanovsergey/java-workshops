import java.io.Serializable;
import java.util.Objects;

// Просто тестовый интерфейс
interface SerialMax extends Serializable{ }

public class Base implements SerialMax {
    private final String description;

    public Base(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Base base = (Base) o;
        return Objects.equals(description, base.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }

    @Override
    public String toString() {
        return "Base{" +
                "description='" + description + '\'' +
                '}';
    }
}