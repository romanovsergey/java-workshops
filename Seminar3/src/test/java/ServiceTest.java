import org.junit.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class ServiceTest {

    @Test
    public void getInfoTest() {
        List<Base> in = new ArrayList<>(Arrays.asList(
                new Derived("Основное описание", "Доп описание"),
                new Base("Просто описание")));

        assertEquals(new ArrayList<>(Arrays.asList(
                "Основное описание, Доп описание",
                "Просто описание")),
                Service.getInfo(in));
    }

    // Интерфейсы для тестирования

    @FunctionalInterface
    interface MyPredicate {
        boolean test(Integer value);

        default double test2() {
            return 0.009;
        }
    }

    interface MyPredicate2 {
        default int cond(double x) {
            return (int) x;
        }
    }

    interface MyPredicate3 {
        boolean test(Integer value);

        float test2();

        static int test3() {
            return 0;
        }

        static int test4() {
            return 1;
        }
    }


    @Test
    public void isFuncInterfaceTest() {
        assertFalse(Service.isFuncInterface(Base.class));
        assertFalse(Service.isFuncInterface(Derived.class));
        assertTrue(Service.isFuncInterface(MyPredicate.class));
        assertFalse(Service.isFuncInterface(MyPredicate2.class));
        assertFalse(Service.isFuncInterface(MyPredicate3.class));
    }

    static class NotSerializable {
    }

    @Test
    public void isSerializableTest() {
        assertTrue(Service.isSerializable(new Base("")));
        assertTrue(Service.isSerializable(new Derived("", "")));
        assertFalse(Service.isSerializable(new NotSerializable()));
    }

    @Test
    public void havingStaticMethodTest() {
        assertEquals(new ArrayList<>(Collections.singletonList("ServiceTest.MyPredicate3")),
                Service.havingStaticMethod(new ArrayList<>(Arrays.asList(
                        Base.class, Derived.class, MyPredicate.class, MyPredicate2.class, MyPredicate3.class))));
    }
}