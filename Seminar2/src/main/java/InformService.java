import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InformService {

    @NotNull
    public static List<String> listDiscip(@NotNull InformStudDiscip ref) {
        List<String> out = new ArrayList<>(ref.size());
        for (TableRow row : ref) {
            out.add(row.getDiscipline());
        }
        return out;
    }

    public static int hoursTotalAllDiscip(@NotNull InformStudDiscip ref) {
        int sum = 0;
        for (TableRow row : ref) {
            int hours = row.getHours();
            if (Double.isNaN(hours))
                throw new IllegalArgumentException("Невозможно распознать количество часов!");
            sum += hours;
        }
        return sum;
    }

    public static double averageMark(@NotNull InformStudDiscip ref) {
        int sum = 0;
        int count = 0;
        for (TableRow row : ref) {
            int grade = row.getGrade().getValue();
            if (grade > 2) {
                sum += grade;
                count++;
            }
        }
        return (double) sum / count;
    }

    @NotNull
    public static Map<String, TableRow.Grades> mapDiscipIntoGrades(@NotNull InformStudDiscip ref) {
        Map<String, TableRow.Grades> out = new HashMap<>();
        for (TableRow row : ref) {
            out.put(row.getDiscipline(), row.getGrade());
        }
        return out;
    }

    @NotNull
    public static Map<TableRow.Grades, List<String>> mapGradesIntoListOfDiscip(@NotNull InformStudDiscip ref) {
        Map<TableRow.Grades, List<String>> out = new HashMap<>();
        for (TableRow row : ref) {
            TableRow.Grades curGrade = row.getGrade();
            if (!out.containsKey(curGrade))
                out.put(row.getGrade(), new ArrayList<>());
            out.get(curGrade).add(row.getDiscipline());
        }
        return out;
    }
}