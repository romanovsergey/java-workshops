import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Objects;

public class TableRow {
    private final String discipline;
    private final int hours;
    private final Grades grade;

    public enum Grades {
        POOR(2, "Плохо"), SATISFACTORY(3, "Удовлетворительно"),
        GOOD(4, "Хорошо"), EXCELLENT(5, "Отлично");

        private final String str;
        private final int val;

        Grades(int value, String string) {
            val = value;
            str = string;
        }

        public String getString() {
            return str;
        }

        public int getValue() {
            return val;
        }

        @Nullable
        @Contract(pure = true)
        public static Grades valueOf(int val) {
            switch (val) {
                case 2:
                    return POOR;
                case 3:
                    return SATISFACTORY;
                case 4:
                    return GOOD;
                case 5:
                    return EXCELLENT;
                default:
                    return null;
            }
        }
    }

    @Contract(pure = true)
    public TableRow(@NotNull String discipline, @NotNull String hours, @NotNull String grade) {
        this.discipline = discipline.trim();
        if (this.discipline.length() == 0)
            throw new IllegalArgumentException("Дисциплина должна состоять минимум из одного символа!");
        this.hours = Integer.parseInt(hours.trim());
        if (Double.isNaN(this.hours) || this.hours < 0)
            throw new IllegalArgumentException("Некорректное значение часов!");
        this.grade = stringToGrade(grade);
    }

    private static Grades stringToGrade(@NotNull String gradeStr) {
        int grade;
        try {
            grade = Integer.parseInt(gradeStr.trim());
        } catch (NumberFormatException e) {
            switch (gradeStr.trim().toLowerCase()) {
                case "плохо":
                    return Grades.POOR;
                case "удовлетворительно":
                    return Grades.SATISFACTORY;
                case "хорошо":
                    return Grades.GOOD;
                case "отлично":
                    return Grades.EXCELLENT;
                default:
                    throw new IllegalArgumentException("Невозможно распознать оценку!");
            }
        }
        if (grade < 2 || grade > 5)
            throw new IllegalArgumentException("Некорректная оценка в 5-бальной системе!");
        return Grades.valueOf(grade);
    }

    public String getDiscipline() {
        return discipline;
    }

    public int getHours() {
        return hours;
    }

    public Grades getGrade() {
        return grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableRow tableRow = (TableRow) o;
        return hours == tableRow.hours &&
                Objects.equals(discipline, tableRow.discipline) &&
                grade == tableRow.grade;
    }

    @Override
    public int hashCode() {
        return Objects.hash(discipline, hours, grade);
    }

    @NotNull
    @Contract(pure = true)
    public static String[] getTheTitle() {
        return new String[]{"Название дисциплины", "Кол-во часов", "Оценка"};
    }

    @Override
    public String toString() {
        return discipline + "\t" +
                hours + "\t" +
                grade.getString() +
                "\n";
    }
}