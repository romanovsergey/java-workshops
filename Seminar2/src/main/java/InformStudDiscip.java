import org.jetbrains.annotations.NotNull;
import java.util.*;

public class InformStudDiscip implements Iterable<TableRow> {
    private final String name, university, faculty, specialty, perOfStudy;
    private final List<TableRow> tabular;

    public InformStudDiscip(String name, String university, String faculty, String specialty, String perOfStudy, @NotNull TableRow... tabular) {
        this.name = name;
        this.university = university;
        this.faculty = faculty;
        this.specialty = specialty;
        this.perOfStudy = perOfStudy;
        this.tabular = new ArrayList<>(tabular.length);
        this.tabular.addAll(Arrays.asList(tabular));
    }

    public String getName() {
        return name;
    }

    public String getUniversity() {
        return university;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getSpecialty() {
        return specialty;
    }

    public String getPerOfStudy() {
        return perOfStudy;
    }

    public int size() {
        return tabular.size();
    }

    @NotNull
    @Override
    public Iterator<TableRow> iterator() {
        return tabular.listIterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InformStudDiscip that = (InformStudDiscip) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(university, that.university) &&
                Objects.equals(faculty, that.faculty) &&
                Objects.equals(specialty, that.specialty) &&
                Objects.equals(perOfStudy, that.perOfStudy) &&
                Objects.equals(tabular, that.tabular);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, university, faculty, specialty, perOfStudy, tabular);
    }

    @Override
    public String toString() {
        return "InformStudDiscip{" +
                "name='" + name + '\'' +
                ", university='" + university + '\'' +
                ", faculty='" + faculty + '\'' +
                ", specialty='" + specialty + '\'' +
                ", perOfStudy='" + perOfStudy + "'\n" +
                Arrays.toString(TableRow.getTheTitle()) + "\n" +
                tabular.toString() +
                '}';
    }
}