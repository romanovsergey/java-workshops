import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class InformServiceTest {
    InformStudDiscip isd;

    @Before
    public void setUp() {
        isd = new InformStudDiscip("Олегов Олег Олегович", "OmSU", "IMIT", "Applied Mathematics", "01.09.2015 - 31.08.2019",
                new TableRow("Мат. Анализ", "145 ", "3"),
                new TableRow("Физ-ра ", " 202 ", "хорошо"),
                new TableRow("Информатика ", "180", "5"),
                new TableRow(" Алгем  ", "243", "4"),
                new TableRow("Экономика", " 546 ", "Удовлетворительно"),
                new TableRow("Спец. курс", "15", "ПЛОХО"));
    }

    @Test
    public void listDiscipTest() {
        assertEquals(new ArrayList<>(Arrays.asList("Мат. Анализ", "Физ-ра", "Информатика", "Алгем", "Экономика", "Спец. курс")),
                InformService.listDiscip(isd));
    }

    @Test
    public void hoursTotalAllDiscipTest() {
        assertEquals(145 + 202 + 180 + 243 + 546 + 15, InformService.hoursTotalAllDiscip(isd));
    }

    @Test
    public void averageMarkTest() {
        assertEquals((double) (3 + 4 + 5 + 4 + 3) / 5, InformService.averageMark(isd), 1E-9);
    }

    @Test
    public void mapDiscipIntoGradesTest() {
        HashMap<String, TableRow.Grades> otv = new HashMap<>();
        otv.put("Мат. Анализ", TableRow.Grades.SATISFACTORY);
        otv.put("Физ-ра", TableRow.Grades.GOOD);
        otv.put("Информатика", TableRow.Grades.EXCELLENT);
        otv.put("Алгем", TableRow.Grades.GOOD);
        otv.put("Экономика", TableRow.Grades.SATISFACTORY);
        otv.put("Спец. курс", TableRow.Grades.POOR);
        assertEquals(otv, InformService.mapDiscipIntoGrades(isd));
    }

    @Test
    public void mapGradesIntoListOfDiscipTest() {
        HashMap<TableRow.Grades, List<String>> otv = new HashMap<>();
        otv.put(TableRow.Grades.EXCELLENT, new ArrayList<>(Collections.singletonList("Информатика")));
        otv.put(TableRow.Grades.GOOD, new ArrayList<>(Arrays.asList("Физ-ра", "Алгем")));
        otv.put(TableRow.Grades.SATISFACTORY, new ArrayList<>(Arrays.asList("Мат. Анализ", "Экономика")));
        otv.put(TableRow.Grades.POOR, new ArrayList<>(Collections.singletonList("Спец. курс")));
        assertEquals(otv, InformService.mapGradesIntoListOfDiscip(isd));
    }
}