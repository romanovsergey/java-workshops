import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TableRowTest {
    TableRow tr;

    @Test
    public void norm() {
        tr = new TableRow(" Мат. Анализ", "145 ", "2");
        tr = new TableRow("Физ-ра", " 202 ", "плохо ");
        tr = new TableRow("Мат. Анализ", "145 ", "3");
        tr = new TableRow("Спец. курс ", " 202 ", "Хорошо");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failHours() {
        tr = new TableRow("Информатика ", "сто восемьдесят", " 4  ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failHours2() {
        tr = new TableRow(" Алгем  ", "-987", "2");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failGrade() {
        tr = new TableRow("Физ-ра", " 200 ", "7");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failGrade2() {
        tr = new TableRow("Экономика", " 546 ", "-3 ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void failDiscip() {
        tr = new TableRow("   ", "5", "5");
    }
}