import buffer.PriorityBuffer;
import buffer.SimpleBuffer;
import processor.*;
import org.junit.Test;
import task.PriorityTask;

import static org.junit.Assert.*;

public class TaskProcessorTest {
    @Test
    public void process() {
        PriorityBuffer pb = new PriorityBuffer();
        pb.add(new PriorityTask(55, 1, 2, 3, 4));                   //3
        pb.add(new PriorityTask(1, -2, 2, 5, -5, 0));               //1
        pb.add(new PriorityTask(30, 90, 123, -23, -40, 2, -12));    //2
        SimpleTaskProcessor stp = new SimpleTaskProcessor(pb);
        assertEquals(Integer.valueOf(0), stp.process());
        assertEquals(Integer.valueOf(140), stp.process());
        assertEquals(Integer.valueOf(10), stp.process());
    }
}