import buffer.*;
import org.junit.Before;
import org.junit.Test;
import task.ITask;
import task.PriorityTask;
import task.Task;

import static org.junit.Assert.*;

public class BufferTest {
    PriorityBuffer pb;
    SimpleBuffer sb;

    @Before
    public void setUp() {
        pb = new PriorityBuffer();
        sb = new SimpleBuffer();
    }

    @Test
    public void addTest() {
        Task t1 = new Task(0, 0, 0);
        PriorityTask t2 = new PriorityTask(9, 1, 2, 3, 4, 5);
        PriorityTask t3 = new PriorityTask(5, 90, 75);

        assertTrue(sb.isEmpty() && pb.isEmpty());           //isEmpty
        sb.add(t1);
        sb.add(t2);
        sb.add(t3);
        pb.add(t2);
        pb.add(t3);
        assertFalse(sb.isEmpty() || pb.isEmpty());          //isEmpty
        assertEquals(3, sb.size());                         //add
        assertEquals(2, pb.size());                         //add

        ITask[] arrT = new Task[sb.size()];
        ITask[] arrPT = new PriorityTask[pb.size()];
        for (int i = 0; i < 3; i++)
            arrT[i] = sb.get();
        for (int i = 0; i < 2; i++)
            arrPT[i] = pb.get();
        assertArrayEquals(new ITask[]{new Task(0, 0, 0), new PriorityTask(9, 1, 2, 3, 4, 5), new PriorityTask(5, 90, 75)}, arrT);   //get
        assertArrayEquals(new ITask[]{new PriorityTask(5, 90, 75), new PriorityTask(9, 1, 2, 3, 4, 5)}, arrPT);                            //get
    }

    @Test
    public void clearTest() {
        sb.add(new Task(0, 0, 0));
        pb.add(new Task(0, 0, 0));
        sb.clear();
        pb.clear();
        assertTrue(sb.isEmpty() && pb.isEmpty());
    }
}