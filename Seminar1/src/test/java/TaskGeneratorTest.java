import buffer.PriorityBuffer;
import buffer.SimpleBuffer;
import generator.*;
import org.junit.Before;
import org.junit.Test;
import task.PriorityTask;
import task.Task;

import static org.junit.Assert.*;

public class TaskGeneratorTest {
    PriorityBuffer pb;
    SimpleBuffer sb;
    SimpleTaskGenerator stg;
    PriorityTaskGenerator ptg;

    @Before
    public void setUp() {
        pb = new PriorityBuffer();
        sb = new SimpleBuffer();
        stg = new SimpleTaskGenerator(sb, -2, 5);
        ptg = new PriorityTaskGenerator(pb, 3, 10, 99);
    }

    @Test
    public void generateTest() {
        stg.generate();
        ptg.generate();

        assertEquals(new Task(-2, -1, 0, 1, 2), sb.get());
        assertEquals(new PriorityTask(99, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12), pb.get());

        ptg.withStartValue(0);
        ptg.withAmount(3);
        ptg.withPriority(6);
        ptg.generate();
        assertEquals(new PriorityTask(6, 0, 1, 2), pb.get());
    }
}