package generator;

import buffer.IBuffer;
import buffer.SimpleBuffer;
import task.Task;

public class SimpleTaskGenerator implements ITaskGenerator {
    private SimpleBuffer buf;
    private int startValue, amount;

    public SimpleTaskGenerator(SimpleBuffer buf, int startValue, int amount) {
        this.buf = buf;
        this.startValue = startValue;
        this.amount = amount;
    }

    @Override
    public void generate() {
        int[] data = new int[amount];
        for (int i = 0; i < amount; i++)
            data[i] = startValue + i;
        buf.add(new Task(data));
    }

    public SimpleTaskGenerator withStartValue(int startValue) {
        this.startValue = startValue;
        return this;
    }

    public SimpleTaskGenerator withAmount(int amount) {
        this.amount = amount;
        return this;
    }
}