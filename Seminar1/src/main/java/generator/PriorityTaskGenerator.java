package generator;

import buffer.PriorityBuffer;
import task.PriorityTask;

public class PriorityTaskGenerator implements ITaskGenerator {
    private PriorityBuffer buf;
    private int startValue, amount;
    private int priority;

    public PriorityTaskGenerator(PriorityBuffer buf, int startValue, int amount, int priority) {
        this.buf = buf;
        this.startValue = startValue;
        this.amount = amount;
        this.priority = priority;
    }

    @Override
    public void generate() {
        int[] data = new int[amount];
        for (int i = 0; i < amount; i++)
            data[i] = startValue + i;
        buf.add(new PriorityTask(priority, data));
    }

    public PriorityTaskGenerator withPriority(int p) {
        this.priority = p;
        return this;
    }

    public PriorityTaskGenerator withStartValue(int startValue) {
        this.startValue = startValue;
        return this;
    }

    public PriorityTaskGenerator withAmount(int amount) {
        this.amount = amount;
        return this;
    }
}