package buffer;
import task.ITask;

public interface IBuffer {
    void add(ITask t);
    ITask get();
    int size();
    boolean isEmpty();
    void clear();
}