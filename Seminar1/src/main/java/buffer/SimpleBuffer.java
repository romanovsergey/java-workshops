package buffer;

import task.ITask;
import task.Task;
import java.util.ArrayDeque;
import java.util.Queue;

public class SimpleBuffer implements IBuffer {
    private Queue<Task> q;

    public SimpleBuffer() {
        this.q = new ArrayDeque<Task>();
    }

    public void add(ITask t) {
        q.add((Task) t);
    }

    public Task get() {
        return (Task) q.remove();
    }

    public int size() {
        return q.size();
    }

    public boolean isEmpty() {
        return q.isEmpty();
    }

    public void clear() {
        q.clear();
    }
}