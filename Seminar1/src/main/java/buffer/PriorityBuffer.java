package buffer;

import task.*;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityBuffer implements IBuffer {
    private Queue<PriorityTask> q;

    public PriorityBuffer() {
        this.q = new PriorityQueue<>();
    }

    public void add(ITask t) {
        q.add((PriorityTask) t);
    }

    public PriorityTask get() {
        return q.remove();
    }

    public int size() {
        return q.size();
    }

    public boolean isEmpty() {
        return q.isEmpty();
    }

    public void clear() {
        q.clear();
    }
}