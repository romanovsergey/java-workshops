package processor;

public interface ITaskProcessor {
    Integer process();
}