package processor;

import buffer.IBuffer;
import task.ITask;
import java.util.NoSuchElementException;

public class SimpleTaskProcessor implements ITaskProcessor {
    private IBuffer buf;

    public SimpleTaskProcessor(IBuffer buf) {
        this.buf = buf;
    }

    @Override
    public Integer process() {
        try {
            int sum = 0;
            ITask t = buf.get();
            for (int elem : t.getData()) {
                sum += elem;
            }
            return sum;
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}