package task;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;

public class PriorityTask implements ITask, Comparable<PriorityTask> {
    private int[] data;
    private int priority;

    public PriorityTask(int priority, int... data) {
        this.data = data;
        this.priority = priority;
    }

    public int[] getData() {
        return data;
    }

    @Override
    public int compareTo(@NotNull PriorityTask pt) {
        return priority - pt.priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriorityTask that = (PriorityTask) o;
        return priority == that.priority &&
                Arrays.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(priority);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public String toString() {
        return "PriorityTask{" +
                "data=" + Arrays.toString(data) +
                ", priority=" + priority +
                '}';
    }
}